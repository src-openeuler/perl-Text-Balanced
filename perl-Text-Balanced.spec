Name:           perl-Text-Balanced
Version:        2.06
Release:        2
Summary:        Extract delimited text sequences from strings
License:        GPL-1.0-or-later OR Artistic-1.0-Perl
URL:            https://metacpan.org/release/Text-Balanced
Source0:        https://cpan.metacpan.org/authors/id/S/SH/SHAY/Text-Balanced-%{version}.tar.gz
BuildArch:      noarch

BuildRequires:  coreutils, findutils, make
BuildRequires:  perl-interpreter
BuildRequires:  perl-generators
BuildRequires:  perl(ExtUtils::MakeMaker)
BuildRequires:  perl(strict)
BuildRequires:  perl(vars)
Conflicts:      perl < 4:5.22.0-347

%description
These Perl subroutines may be used to extract a delimited substring, possibly
after skipping a specified prefix string.

%prep
%setup -q -n Text-Balanced-%{version}

%build
perl Makefile.PL INSTALLDIRS=vendor NO_PACKLIST=1
%make_build

%install
make pure_install DESTDIR=$RPM_BUILD_ROOT
%{_fixperms} %{buildroot}/*

%check
make test

%files
%doc README
%license LICENCE
%{perl_vendorlib}/*

%package_help
%files help
%doc Changes
%{_mandir}/man3/*

%changelog
* Sun Jan 19 2025 Funda Wang <fundawang@yeah.net> - 2.06-2
- drop useless perl(:MODULE_COMPAT) requirement

* Mon Oct 24 2022 huyubiao <huyubiao@huawei.com> - 2.06-1
- upgrade version to 2.06

* Fri Jan 29 2021 liudabo <liudabo1@huawei.com> - 2.04-1
- upgrade version to 2.04

* Sat Dec 21 2019 openEuler Buildteam <buildteam@openeuler.org> - 2.03-420
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:change mod of files

* Sun Sep 29 2019 shenyangyang <shenyangyang4@huawei.com> - 2.03-419
- Type:NA
- ID:NA
- SUG:NA
- DESC:move the directory of README

* Mon Sep 23 2019 shenyangyang<shenyangyang4@huawei.com> - 2.03-418
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:revise requires of perl

* Fri Aug 30 2019 openEuler Buildteam <buildteam@openeuler.org> - 2.03-417
- Package init
